package OliveEdit;

require Exporter;
use warnings;
use strict;

our @ISA       = qw(Exporter);
our @EXPORT    = qw( &deletefeed &editfeed );

#--------------------------------------------------------------

sub editfeed {
    my $cui = shift;
    my $w   = $cui->userdata->{wins};
    my $c   = $cui->userdata->{c};

    if($cui->getobj('editwin')) {
        $cui->getobj('editwin')->focus;
        $cui->getobj('editwin')->draw;
        return;
    }

    # build hash of options from config object
    my ($values,$labels) = feedlist($c);

    my $edit = $cui->add('editwin', 'Window',
                         -border   => 1,
                         -bfg      => 'blue',
                         -title    => 'Edit Feeds',
                         -height   => 18,
                         -width    => 30,
                         -centered => 1,
                        );
    $edit->{l1} = $edit->add(undef, 'Label',
                             -text => 'Feed              Status',
                             -x    => 2,
                             -y    => 1,
                             -bold => 1,
        );
    $edit->{list} = $edit->add('edli', 'Listbox',
                               -values => $values,
                               -labels => $labels,
                               -height => 10,
                               -width  => 25,
                               -y      => 3,
                               -x      => 2,
                               -vscrollbar => 1,
                               -onchange   => sub { editsingle($cui,$edit->{list}) },
        );
    $edit->{okb} = $edit->add('edok', 'Buttonbox',
                              -y       => -2,
                              -x       => 20,
                              -buttons => [ { -label   => '< OK >',
                                              -value   => 1,
                                              -onpress => sub { $edit->loose_focus;
                                                                $c->write;
                                                                $cui->enable_timer('clock');
                                                                $cui->delete('editwin');
                                                                $cui->draw },
                                            }
                              ],
        );
    $cui->disable_timer('clock');
    $edit->draw;
    $edit->focus;
}

sub editsingle {
    my ($cui,$e) = @_;
    my $c = $cui->userdata->{c};
    my $nick = $e->get;
    return unless (defined $nick);

    my %f; my %d; my %fl;
    $f{0}  = 0;
    $f{0}  = 1 if $c->{feeds}{$nick}{force};
    $d{0}  = 0;
    $d{0}  = 1 if $c->{feeds}{$nick}{dormant};
    $fl{0} = 0;
    $fl{0} = 1 if $c->{feeds}{$nick}{flag};

    if($cui->getobj('feed')) {
        $cui->getobj('feed')->focus;
        $cui->getobj('feed')->draw;
        return;
    }

    my $fw = $cui->add('feed', 'Window',
                         -border   => 1,
                         -bfg      => 'blue',
                         -title    => "Edit $c->{feeds}{$nick}{disp}",
                         -height   => 15,
                         -width    => 60,
                         -centered => 1,
                        );
    $fw->{l1} = $fw->add(undef, 'Label',
                         -text => "Location:",
                         -x    => 1,
                         -y    => 1,
        );
    $fw->{l2} = $fw->add(undef, 'Label',
                         -text => "Nickname:",
                         -x    => 1,
                         -y    => 3,
        );
    $fw->{loc} = $fw->add('fwlo', 'TextEntry',
                          -x       => 11,
                          -y       => 1,
                          -width   => 46,
                          -reverse => 1,
                          -text    => $c->{feeds}{$nick}{feed},
        );
    $fw->{nic} = $fw->add('fwnn', 'TextEntry',
                          -x         => 11,
                          -y         => 3,
                          -width     => 17,
                          -maxlength => 16,
                          -reverse   => 1,
                          -text      => $c->{feeds}{$nick}{disp},
        );
    $fw->{ffl} = $fw->add('fwfl', 'Listbox',
                          -values => [ 1 ],
                          -labels => { 1  => 'Flag this feed' },
                          -selected => \%fl,
                          -multi    => 1,
                          -height   => 1,
                          -width    => 27,
                          -y        => 5,
                          -x        => 11,
        );
    $fw->{frc} = $fw->add('fwfr', 'Listbox',
                          -values => [ 1 ],
                          -labels => { 1  => 'Force-poll this feed' },
                          -selected => \%f,
                          -multi    => 1,
                          -height   => 1,
                          -width    => 27,
                          -y        => 7,
                          -x        => 11,
        );
    $fw->{fdr} = $fw->add('fwdr', 'Listbox',
                          -values => [ 1 ],
                          -labels => { 1  => 'Feed is dormant' },
                          -selected => \%d,
                          -multi    => 1,
                          -height   => 1,
                          -width    => 27,
                          -y        => 9,
                          -x        => 11,
        );
    $fw->{okb} = $fw->add('fwok', 'Buttonbox',
                        -y       => -2,
                        -x       => 40,
                        -buttons => [ { -label   => '< OK >',
                                        -value   => 1,
                                        -onpress => sub { &OliveFeed::storefeed($cui,
                                                                    $fw->{loc}->get,
                                                                    $fw->{nic}->get,
                                                                    $fw->{frc}->get || 0,
                                                                    $fw->{fdr}->get || 0,
                                                                    $fw->{ffl}->get || 0,
                                                                    $nick);
                                                          unless ($fw->{fdr}->get) {
                                                              &OliveFeed::feedpoll($cui); 
                                                              &OliveFeed::refreshlist($cui);
                                                          }
                                                          $fw->loose_focus;
                                                          $cui->delete('feed');
                                                          $e->set_selection(-10061);
                                                          $cui->delete('editwin');
                                                          editfeed($cui) }
                                      },
                                      { -label   => '< Cancel >',
                                        -value   => 0,
                                        -onpress => sub { $fw->loose_focus;
                                                          $cui->delete('feed');
                                                          $e->set_selection(-10061);
                                                          $cui->draw },
                                      },
                                      
                          ],
        );
    $fw->draw;
    $fw->{loc}->focus;
}








sub deletefeed {
    my $cui = shift;
    my $w   = $cui->userdata->{wins};
    my $c   = $cui->userdata->{c};

    if($cui->getobj('opts')) {
        $cui->getobj('opts')->focus;
        $cui->getobj('opts')->draw;
        return;
    }

    # build hash of options from config object
    my ($values,$labels) = feedlist($c);

    my $opts = $cui->add('delf', 'Window',
                         -border   => 1,
                         -bfg      => 'blue',
                         -title    => 'Remove Feeds',
                         -height   => 18,
                         -width    => 34,
                         -centered => 1,
                        );
    $opts->{l1} = $opts->add(undef, 'Label',
                             -text => 'Sel',
                             -x    => 2,
                             -y    => 1,
                             -bold => 1,
        );
    $opts->{l2} = $opts->add(undef, 'Label',
                             -text => 'Feed              Status',
                             -x    => 6,
                             -y    => 1,
                             -bold => 1,
        );
    $opts->{list} = $opts->add('obli', 'Listbox',
                               -values => $values,
                               -labels => $labels,
                               -multi  => 1,
                               -height => 11,
                               -width  => 29,
                               -y      => 2,
                               -x      => 2,
                               -vscrollbar => 1,
        );
    $opts->{okb} = $opts->add('obok', 'Buttonbox',
                              -y       => -2,
                              -x       => 24,
                              -buttons => [ { -label   => '< OK >',
                                              -value   => 1,
                                              -onpress => sub { $opts->loose_focus;
                                                                saveopts($cui,$opts->{list});
                                                                $c->write;
                                                                $cui->enable_timer('clock');
                                                                $cui->delete('delf');
                                                                &OliveFeed::feedpoll($cui);
                                                                &OliveFeed::refreshlist($cui);
                                                                $cui->draw },
                                            } 
                              ],
        );
    $cui->disable_timer('clock');
    $opts->draw;
    $opts->focus;
}

sub saveopts {
    my ($cui,$o) = @_;
    my $c = $cui->userdata->{c};
    my $d = $cui->userdata->{dbh};
    my @opts    = $o->get;
    
    # destroy feeds
    foreach my $opt (@opts) {
        # clear out db entries
        my $statement = "DELETE FROM stories WHERE nick = '$opt'";
        my $sth = $d->prepare($statement);
        $sth->execute;
        # delete from HoH
        delete $c->{feeds}{$opt};
        # delete feed files
        my $feed  = $cui->userdata->{feeds} . "/$opt";
        my $cache = $cui->userdata->{feeds} . "/$opt.cache";
        unlink ($feed, $cache);
    }
}


sub feedlist {
    my ($c) = @_;
    my @v = ();
    my %l = ();
    my $i = 0;
    foreach my $opt (sort(keys %{$c->{feeds}})) {
        $v[$i] = $opt;
        my $label = $c->{feeds}{$opt}{disp} .
            ' ' x (21 - 3 - length($c->{feeds}{$opt}{disp})) .
            ($c->{feeds}{$opt}{flag}    ? 'f' : '-') .
            ($c->{feeds}{$opt}{force}   ? 'p' : '-') .
            ($c->{feeds}{$opt}{dormant} ? 'd' : '-');
        $l{$v[$i]} = $label;
        $i++;
    }
    return(\@v,\%l);
}

1;

=head1 COPYRIGHT & LICENSE

Copyright 2005,2006 Shawn Boyette, All Rights Reserved.

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
