package OliveOPML;

=head1 NAME

OliveOPML - Badly import OPML files

=head1 DESCRIPTION

This library very stupidly attempts to read OPML files. At the moment
it handles Bloglines and NetNewsWire output. If you want others added,
send patches or an exported OPML file.

=cut

require Exporter;
use warnings;
use strict;
use Config::YAML;
use DBI;
use Digest::MD5 qw( md5_hex );
use XML::Simple;

our @ISA       = qw(Exporter);
our @EXPORT    = qw( &opmlimport );

my $c = Config::YAML->new( config => "$ENV{HOME}/.olive/olive.yaml" );
my $i = my $j = 0;
my $type = '';

sub opmlimport {
    my $opml = shift;
    my $feeds;

    my $x = XML::Simple->new;
    my $f = $x->XMLin($opml);

    print "Beginning OPML import.\n";

    if (ref $f->{body}{outline} eq "ARRAY") {
        $feeds = $f->{body}{outline};
        $type = 'nnw';
    } elsif ( ref $f->{body}{outline} eq "HASH") {
        $feeds = $f->{body}{outline}{outline};
        $type = 'bl';
    } else {
        die "Can't find the freakin' feeds list!\n";
    }

    if ($type eq 'nnw') {
        map { extract($_) } (@{$feeds});
    } elsif ($type eq 'bl') {
        map { extract_bl($_) } (@{$feeds});
    }
 
    print "$j of $i feeds imported\n";
    print "See ~/.olive/errors.log for more information\n" if ($i != $j);
}

sub extract {
    my $outline = shift;

    my $text = my $url = 0;

    if ($outline->{text}) {
        $text = $outline->{text};
    } elsif ($outline->{title}) { 
        $text = $outline->{title}
    } else {
        1;
    }

    print "$text\n";

    if ($outline->{xmlUrl}) {
        $url = $outline->{xmlurl};
    } elsif ($outline->{url}) {
        $url = $outline->{url};
    }

    # throw back "container" outlines with no feed attached
    return unless ($text && $url);

    storefeed($text,$url);
}

sub extract_bl {
    my $outline = shift;
    my $text = my $url = 0;

    $text = $outline->{title};

    if ($outline->{xmlUrl}) {
        $url = $outline->{xmlUrl};
    } else {
        if (ref $outline->{outline} eq 'HASH') {
            extract_bl($outline->{outline});
        } else {
            print "Descending into $text...\n";
            map { extract_bl($_) } (@{$outline->{outline}});
        }
    }

    # throw back "container" outlines with no feed attached
    return unless ($text && $url);

    storefeed($text,$url);
}

sub storefeed {
    my ($disp,$feed) = @_;

    $i++;

    # trim whitespace
    $feed =~ s/^\s+//;
    $feed =~ s/\s+$//;
    $disp =~ s/^\s+//;
    $disp =~ s/\s+$//;
    $disp = substr($disp,0,16);

    # save original nick for display and sanitize
    my $nick = $disp;
    $nick =~ s/\s/_/g;
    $nick =~ s/\W//g;
    $nick =~ s/_+/_/g;
    $nick =~ s/_$//;
    $nick = md5_hex($disp) if ($nick eq '');
    $nick = lc($nick);
    
    # check for dupe nicks
    while (defined $c->{feeds}{$nick}) {
        print STDERR "The nick '$disp' is in use or reduces to a nick which is in use ($feed).\n";
        return;
    }        

    # check for dupe feeds
    foreach my $f (keys %{$c->{feeds}}) {
        if ( ($f ne $nick) && ($feed eq $c->{feeds}{$f}{feed}) ) {
            my $disp = $c->{feeds}{$f}{disp};
            print STDERR "You're already subscribed to $feed as '$disp'\n";
            return;
        }
    }

    # everything looks okay. store it.
    $c->{feeds}{$nick}{feed}    = $feed;
    $c->{feeds}{$nick}{disp}    = $disp;
    $c->{feeds}{$nick}{force}   = 0;
    $c->{feeds}{$nick}{dormant} = 0;
    $c->{feeds}{$nick}{last}    = 0;
    $c->{feeds}{$nick}{ttl}     = 0;
    $c->write;
    $j++;
}

=head1 COPYRIGHT & LICENSE

Copyright 2005,2006 Shawn Boyette, All Rights Reserved.

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
