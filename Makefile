DESTDIR = 
PREFIX  = /usr/local
BINDIR  = $(PREFIX)/bin
LIBDIR  = $(PREFIX)/share/olive
DOCDIR  = $(PREFIX)/share/doc/olive
PERL    = $(shell which perl)

# DO NOT EDIT BELOW THIS LINE

DEPS    = Config::YAML Curses::UI Date::Calc DBI DBD::SQLite \
	  LWP::UserAgent XML::Parser XML::Simple \
	  Log::Dispatch::File

MODULES = OliveEdit.pm OliveMisc.pm OliveStory.pm OliveXML.pm \
	  OliveFeed.pm OliveOpts.pm OliveWindow.pm OliveHTTP.pm \
	  OliveOPML.pm

.PHONY : olive install

olive :
	@echo -n Checking for dependancies... 
	@perl ./depchecks $(DEPS)
	@echo looks good.

install : olive
	@echo Prepping for install.
	@cp olive olive.tmp
	@perl -pi -e 's|^use lib "."|use lib "$(LIBDIR)"|' olive.tmp
	@perl -pi -e 's|"./docs"|"$(DOCDIR)"|' olive.tmp
	@perl -pi -e 's|^#!.+|#!$(PERL)|' olive.tmp
	@echo Creating install directories.
	@rm -rf $(DESTDIR)$(LIBDIR)
	@install -d $(DESTDIR)$(PREFIX) $(DESTDIR)$(BINDIR) $(DESTDIR)$(LIBDIR) $(DESTDIR)$(DOCDIR)
	@echo Installing Olive.
	@install -m 0755 olive.tmp $(DESTDIR)$(BINDIR)/olive
	@install -m 0644 $(MODULES) $(DESTDIR)$(LIBDIR)
	@install -m 0644 ./docs/*html ./docs/*css $(DESTDIR)$(DOCDIR)
	@echo Cleaning up.
	@rm olive.tmp
	@echo Done. Now run $(BINDIR)/olive to get started!

uninstall:
	@echo Uninstalling Olive.
	@rm $(BINDIR)/olive
	@rm -rf $(LIBDIR) $(DOCDIR)
	@echo Done.