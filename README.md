Olive
=====

Welcome to the historical repository for the original incarnation of
the Olive newsreader. It was a Perl program with an ncurses interface.

Olive was the first thing I ever wrote and released which accrued an
actual userbase, and no one could have been more surprised than I when
this happened. The idea of an RSS newsreader was really nice, but I
couldn't for the life of me fathom why every last one of them groups
stories by the site they came from. It seemed obvious to me that the
"correct" way to present news stories was chronologically, in a single
list.

And so, over a few days in April 2005, I wrote one which did just
that. I posted about it to freshmeat, and to my amazement people
started using it, filing bug reports, and coming up with ideas to make
it better.

The high point was getting a message from Professor [Eben
Moglen](http://en.wikipedia.org/wiki/Eben_Moglen), praising my
interface design decisions and offering very thoughtful and kind
criticism and suggestions. Talk about validation!

Eventually, at least one other reader (Google Reader) adopted the
Olivine approach to news, and eventually Olive itself succumbed to
bitrot and to my own move away from Perl 5 as a focus of my
programming work.

Olive will return one day, either as a standalone app or as part of a
larger suite.