package OliveOpts;

require Exporter;
use warnings;
use strict;
use OliveMisc;

our @ISA       = qw(Exporter);
our @EXPORT    = qw( &setopts &saveopts );

#--------------------------------------------------------------
# option handler routines
#--------------------------------------------------------------

sub setopts {
    my $cui = shift;
    my $w   = $cui->userdata->{wins};
    my $c   = $cui->userdata->{c};

    if($cui->getobj('opts')) {
        $cui->getobj('opts')->focus;
        $cui->getobj('opts')->draw;
        return;
    }

    # build hash of enabled options from config object
    my %selected = ();
    my $i = 0;
    foreach my $opt (@{$cui->userdata->{opts}}) {
        $selected{$i} = 1 if $c->{$opt};
        $i++;
    }

    my $opts = $cui->add('opts', 'Window',
                         -border   => 1,
                         -bfg      => 'blue',
                         -title    => 'Options',
                         -height   => 16,
                         -width    => 35,
                         -centered => 1,
                        );
    $opts->{list} = $opts->add('obli', 'Listbox',
                               -values => $cui->userdata->{opts},
                               -labels => {'sls' => 'Show story list status',
                                           'snu' => 'Skip unread on prev/next',
					   'knf' => 'Keep stories until read',
                                           'gsp' => 'Global story paging',
                                           'dst' => 'Hide titlebar',
                                           'coe' => 'Confirm on exit',
                               },
                               -multi    => 1,
                               -selected => \%selected,
                               -height   => 6,
                               -width    => 30,
                               -y        => 1,
                               -x        => 2,
                               -onchange =>  sub { saveopts($cui,$opts->{list}) },
        );
    $opts->{mins} = $opts->add('obmi', 'TextEntry',
                               -x         => 1,
                               -y         => -6,
                               -width     => 4,
                               -maxlength => 3,
                               -reverse   => 1,
                               -text      => $c->{pw},
                               -onchange  => sub { if ($opts->{mins}->get =~ /\D/) {
                                                       $opts->{mins}->text('');
                                                       $opts->{mins}->text($c->{pw}) if $c->{pw};
                                                   } elsif ($opts->{mins}->get == 0) {
                                                       $opts->{mins}->text('');
                                                   } else {
                                                       $c->{pw} = $opts->{mins}->get;
                                                       &OliveMisc::setpollwait($cui);
                                                   }
                                                 },
        );
    $opts->{l3} = $opts->add(undef, 'Label',
                             -text => "Poll wait (in minutes)",
                             -x    => 6,
                             -y    => -6,
        );
    $opts->{secs} = $opts->add('obse', 'TextEntry',
                               -x         => 1,
                               -y         => -5,
                               -width     => 4,
                               -maxlength => 3,
                               -reverse   => 1,
                               -text      => $c->{to},
                               -onchange  => sub { if ($opts->{secs}->get =~ /\D/) {
                                                       $opts->{secs}->text('');
                                                       $opts->{secs}->text($c->{to}) if $c->{to};
                                                   } else {
                                                       $c->{to} = $opts->{secs}->get;
                                                   }
                                                 },
        );
    $opts->{l5} = $opts->add(undef, 'Label',
                             -text => "Fetch timeout (in secs)",
                             -x    => 6,
                             -y    => -5,
        );
    $opts->{www} = $opts->add('owww', 'TextEntry',
                               -x         => 1,
                               -y         => -3,
                               -width     => 16,
                               -reverse   => 1,
                               -text      => $c->{www},
                               -onchange  => sub { $c->{www} = $opts->{www}->get; },
        );
    $opts->{l4} = $opts->add(undef, 'Label',
                             -text => "Link command",
                             -x    => 18,
                             -y    => -3,
        );
    $opts->{okb} = $opts->add('obok', 'Buttonbox',
                              -y       => -1,
                              -x       => 24,
                              -buttons => [ { -label   => '< Save >',
                                              -value   => 1,
                                              -onpress => sub { $opts->loose_focus;
                                                                $c->write;
                                                                $cui->enable_timer('clock');
                                                                $cui->delete('opts');
                                                                $cui->draw },
                                            } 
                              ],
        );
    $cui->disable_timer('clock');
    $opts->draw;
    $opts->focus;
}

sub saveopts {
    my ($cui,$o) = @_;

    my $c = $cui->userdata->{c};
    my @optlist = @{$cui->userdata->{opts}};
    my @opts    = $o->get;
    my %selopts = ();

    # build hash of selections, all turned off
    foreach my $opt (@optlist) {
        $selopts{$opt} = 0;
    }

    # loop through list of all options and list of widget-enabled
    # options. set the ones which match to 1 (on) 
    foreach my $opt (@optlist) {
        foreach my $opt2 (@opts) {
            $selopts{$opt} = 1 if ($opt eq $opt2);
        }
    }

    # now loop through all opts, turning them on or off in the
    # config onject as appropriate
    foreach my $opt (@optlist) {
        $c->{$opt} = $selopts{$opt};
    }
}

=head1 COPYRIGHT & LICENSE

Copyright 2005 Shawn Boyette, All Rights Reserved.

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
