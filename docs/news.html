<html>
<head>
<title>User Manual: News</title>
<link href="./page.css" rel="stylesheet" type="text/css"/>
<link href="./olive.css" rel="stylesheet" type="text/css"/>
<link href="./manual.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<h1>2. Reading News With Olive</h1>

<p>
(Getting Started) <a href="start.html">Prev</a>
| <a href="index.html">Index</a> | <a href="options.html">Next</a>
(Setting Options)
</p>
<hr/>

<h2 id="s10">2.1 The User Interface</h2>
<p>
  Olive does not mimic GUI-style user interfaces by implementing
  text-based menus, controls, and the like. Instead it takes programs
  like <code>mutt</code> as its inspiration and stays out of your way
  as much as possible.  Transient panels and message dialogues aside,
  the Olive UI is composed of 4 components (listed here
  top-to-bottomly): Title Bar, List Pane, Status Line, and Story
  Pane. Only the List Pane and Story Pane can be interacted with in
  any meaningful way.
</p>
<p>
  Focus is shifted between the List Pane and Story Pane with the
  '<code>w</code>' key, but this is rarely needed anymore.
</p>


<h3 id="s11">2.1.1 The Titlebar</h3>
<p>
  The titlebar is simply the top line of the window, used to display
  Olive's name and version number in white-on-blue text.
</p>
<pre class='screen'>
  |
  | <span>Olive b7                                                  </span>
  |
</pre>
<p>
  You can hide the titlebar and have this line of screen real-estate
  for your own use. See <a href="options.html#s24">Section 3.2.4</a>
  for details on how to do this.
</p>


<h3 id="s12">2.1.2 List Pane</h3>
<p>
  The top half of Olive's window is used by the List Pane. This is
  where the stories from the feeds currently in your feed list are
  displayed, one story per line. Each line displays the following
  information:
</p>
<ol>
  <li>Title (taken from RSS data)</li>
  <li>Feed nickname (taken from feed config)</li>
  <li>Age (time since posting, taken from RSS data)</li>
  <li>Starred indicator</li>
  <li>Read/Unread indicator</li>
</ol>
<p>
  Stories are sorted as follows:
</p>
<ol>
  <li>
    New or Unread stories, sorted oldest-to-newest by age
  </li>
  <li>
    Old stories, sorted newest-to-oldest by age
  </li>
</ol>
<p>
  If there are both new and old stories present in the list, a
  separator line of dashes will be displayed between them.
</p>
<pre class='screen'>
                  TITLE                       FEED         AGE   S U
    |------ (Width - 29 chars) -------| |-- 16 chars --| |- 7 -| 1 1

  | Martian rover rolls free of trap    BBC News         00h 52m   @
  | Pacman comes to life virtually      BBC News         00h 38m * @ 
  | Many dead in Nepalese bus blast     BBC News         00h 12m   @
  | ----------------------------------------------------------------
  | China dismisses US Tiananmen call   BBC News         03h 38m * -
  | Japanese firms raise investment     BBC News         04h 12m   -

</pre>
<p>
  The age field displays '<code>HHh MMm</code>' for stories less than
  one day old, '<code>DDd HHh</code>' for stories between 1 and 100
  days old, '<code>___+99d</code>' for stories 100 or more days old,
  and '<code>____???</code>' for stories with no date (or invalid
  dates) in their RSS data.
</p>
<p>
  The Starred indicator displays '<code>*</code>' for stories you have
  flagged as <em>starred</em> and nothing for stories which are
  unstarred. See <a href="#s40">Section 2.4</a> for more information
  on this.
</p>
<p>
  The Read/Unread indicator displays '<code>@</code>' for unread
  stories and '<code>-</code>' for stories which you have already
  read. As you read stories, they will remain in place among the new
  stories until the next feed poll (see
  <a href="options.html#s30">Section 3.3</a>) occurs; they will then
  be sorted with the old stories.
</p>
<p>
  The list cursor is displayed as a bolded reverse-video bar spanning
  the width of the pane.
</p>
<pre class='screen'>
  | Martian rover rolls free of trap    BBC News         00h 52m   @
  | <span class='rv'><b>Pacman comes to life virtually      BBC News         00h 38m * @</b></span> 
  | Many dead in Nepalese bus blast     BBC News         00h 12m   @
  | --------------------------------------------------------------
  | China dismisses US Tiananmen call   BBC News         03h 38m * -
  | Japanese firms raise investment     BBC News         04h 12m   -
</pre>
<p>
  When the cursor is moved from the currently selected story, that
  story is displayed in bold face:
</p>
<pre class='screen'>
  | Martian rover rolls free of trap    BBC News         00h 52m   @
  | <b>Pacman comes to life virtually      BBC News         00h 38m * @</b> 
  | Many dead in Nepalese bus blast     BBC News         00h 12m   @
  | --------------------------------------------------------------
  | <span class='rv'>China dismisses US Tiananmen call   BBC News         03h 38m * -</span>
  | Japanese firms raise investment     BBC News         04h 12m   -
</pre>
<p>
  The cursor will return to the top of the list when
  a <a href="feeds.html#s40">feed poll</a> occurs.
</p>

<h3 id="s13">2.1.3 The Status Line</h3>
<p>
  Between the List Pane and the Story Pane sits the Status Line. It is
  a single line which displays various and sundry informational
  mesasges for you. It cannot be turned off, but its behavior can be
  modified.
</p>
<p>
  Most of the status line is taken up by the Staus Area. If enabled as
  per <a href="options.html#s21">Section 3.2.1</a>, the Status Area
  will display a message in the following format when no other
  activity is happening:
</p>
<pre class='screen'>
  |
  | <span><b>[S: 1/175] [U/N: 21/21] [P: 5 @ 05:45 | 6 since 02:41] [F: F,S]</b>     </span>
  |
</pre>
<p>
  It is read as follows:
</p>
<dl>
  <dt><code>[S: n/n]</code></dt>
  <dd>
    Story count. The first number is the number (in the story list) of
    the location of the list cursor. The second number is the total
    number of stories in the list.
  </dd>
  <dt><code>[U/N: n/n]</code></dt>
  <dd>
    Unread/New count. The first number is how many unread stories
    remain in the list. The second number is now many new stories are
    currently in the list.
  </dd>
  <dt><code>[P: n @ HH:MM | n since HH:MM]</code></dt>
  <dd>
    Polling data. The data on the left side of the pipe is how many
    feeds were refreshed during the last poll, and the time at which
    that poll occurred. The date on the right side is how many
    feeds <b>total</b> have been refreshed since the last user
    activity (presently defined as "reading a story").
  </dd>
  <dt><code>[F: X,Y,..Z]</code></dt>
  <dd>
    Filter status. The comma-separated list of letters indicated which
    filters are active. Current possible values are <code>F</code> for
    flagged feeds and <code>S</code> for starred stories. If no
    filters are active, this block will not display at all.
  </dd>
</dl>
<p>
  When polling is active, the Status Area displays brief messages
  about the polling process (Checking..., Fetching..., etc.) for each
  feed being polled.  When the Story Pane is focused, the Status Area
  displays the title (the actual title, taken from the feed, not the
  nickname displayed in the story list) of the feed from which the
  currently selected story is taken.
</p>
<p>
  The rightmost character of the Status Line is the Focus Pointer. It
  simply points to whichever of the the List Pane (<code>^</code>) or
  the Story Pane (<code>V</code>) is focused.
</p>

<h3 id="s14">2.1.4 Story Pane</h3>
<p>
  The bottom half of Olive's window is used by the Story Pane. This is
  where the content of (and some other information from) the currently
  selected story is displayed.
</p>
<pre class='screen'>
  |             Story Title
  |             -----------
  |
  | -- Header
  |
  | Lorem ipsum dolor sit amet, consectetuer 
  | adipiscing elit. Vivamus rhoncus libero. 
  |
  | Aenean sit amet risus sed felis molestie 
  | hendrerit. Pellentesque feugiat mauris.
  |
  | -- 
  | http://someurl/pathto/story.html
</pre>
<h3 id="s15">2.1.5 Searching</h3>
<p>
  The List and Story panes, as well as the feed Edit and Remove
  panels, have capabilities much like those
  of the Unix utility <code>less</code>.
</p>
<ul>
  <li>Press '<code>/</code>' to start a search, then...</li>
  <li>
    Type your search term and press <code>Enter</code>, which will
    cause the cursor to jump to the first matching line, or
    display <code>Not found</code> if there is no match.
  </li>
  <li>
    Optionally, press '<code>n</code>' to repeat the search forward or
    '<code>N</code>' to repeat it backwards,
  </li>
  <li>
    Press <code>Enter</code> to select the current match and end the
    search or...
  </li>
  <li>
    Press '<code>q</code>' to end the search without selecting
    anything.
  </li>
</ul>




<h2 id="s20">2.2 Reading The News</h2>
<p>
  The selection cursor in the List Pane is moved one line at a time
  with the up and down arrow keys or the '<code>j</code>' and
  '<code>k</code>' keys, Pressing <code>Enter</code> selects the
  current story and causes its contents to be loaded into the Story
  Pane.
</p>
<p>
  Cursor movement and story selection functions are combined in the
  '<code>[</code>' and '<code>]</code>' keys, which select the
  previous and next stories,respectively.
</p>
<p>
  The cursor can also be moved one screen at a time
  with <code>PgUp</code> and <code>PgDn</code>, or to the top and
  bottom of the list with <code>Home</code> and <code>End</code>.
</p>
<p>
  Individual stories can be marked or unmarked as read with the
  '<code>m</code>' and '<code>u</code>' keys. <em>All</em> stories can
  be marked/unmarked at once with '<code>M</code>' and
  '<code>U</code>'.
</p>
<p>
  When the Story Pane is focused, the story text can be scrolled one
  line up or down with the arrow keys or the '<code>j</code>' and
  '<code>k</code>' keys. It can also be scrolled one screen at a time
  with <code>PgUp</code> and <code>PgDn</code>, or to the top and
  bottom with <code>Home</code> and <code>End</code>.
</p>


<h2 id="s30">2.3 Link Handling</h2>
<p>
  Nearly all stories will have a single link URL attached to them
  which leads to a fuller or permanent copy of the story from the
  feed. This URL is displayed after the story text, and can be
  followed by pressing the '<code>l</code>' (ell) key.
</p>
<p>
  Some news stories have URLs embedded in their content, in addition
  to the dedicated link URL. These can be accessed by pressing
  '<code>L</code>', which will pop up a Linklist in the top-right
  corner of the Olive window.
</p>
<pre class='screen'>
    .<span class='rv'>[ Linklist ]</span>-----------------------------------.
    |                                               |
    | [ 6] permalink                               ||
    | <span class='rv'><b>[ 7] incorporated</b>                           </span> ||
    | [ 8] entry                                   <span class='rv'>#</span>|
    | [ 9] discovery                               <span class='rv'>#</span>|
    | [10] Patrick Stewart                         <span class='rv'>#</span>|
    | [11] Circulating intentional data            ||
    |                                               |
    |                                    < Cancel > |
    |                                               |
    '-----------------------------------------------'
</pre>
<p>
  The numbers correspond to markers which are placed in the story
  text, as in:
</p>
<pre class='screen'>
    On the test version of the catalog he has [incorporated][7]
    tagging, which he discusses in a recent [entry][8] on his blog.
</pre>
<p>
  Select a link with the Up/Down arrow keys and
  press <code>Enter</code> to launch an external browser session with
  the URL of that link as the
  target. If <a href="options.html#s23">Global Story Paging</a> has
  not been disabled, the story text can be paged while the Linklist is
  displayed. The linklist will be dismissed when you <code>Tab</code>
  to the "Cancel" button and press <code>Enter</code>.
</p>
  

<h2 id="s40">2.4 Starred Stories and Flagged Feeds</h2>
<p>
  Olive has a pair of selector mechanisms to help you separate the
  wheat from the chaff in your newsreading: starring stories and
  flagging feeds.
</p>
<p>
  Starred stories will remain in your story list even after they have
  "fallen off" the feed they were sourced from. This lets you force
  stories to stick around if you need them for later reference.  The
  star flag of a story is toggled on/off with the '<code>s</code>'
  key.  Once unstarred, a story will be removed from the story
  database if it is no longer in the feed it came from. The secondary
  effect of starring stories is that you can filter the story list to
  show only the currently starred stories with the '<code>S</code>'
  key.
</p>
<p>
  Unlike starred stories, which are immortal, There is nothing special
  about flagged feeds except that you have "flagged" them as special
  in some way. For instance, I flag the feeds which belong to journals
  of my friends, marking them as distinct from the general news feeds
  I subscribe to. The story list can be filtered to show only stories
  from flagged feeds with the '<code>F</code>'
  key. See <a href="feeds.html#s10">Section 4.1</a> for information on
  flagging feeds.
</p>
  The current filtering status will be shown in
  the <a href="#s13">Status Area</a> of the Status Line when filtering
  is active.
</p>

<hr/>
<p>
(Getting Started) <a href="start.html">Prev</a>
| <a href="index.html">Index</a> | <a href="options.html">Next</a>
(Setting Options)
</p>
<hr/>
<p>
$Id: news.html 437 2007-04-30 13:10:36Z mdxi $
</p>
</body>
</html>
